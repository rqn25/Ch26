
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class Display extends JPanel {
	AVLTree<Integer> tree;
	JTextField textfield = new JTextField(5);
	View view = new View();
	JButton button1 = new JButton("Insert");
	JButton button2 = new JButton("Delete");

public static class ViewTree extends JApplet {

public ViewTree() {
	add(new Display(new AVLTree<Integer>()));

	}
	}

public Display(AVLTree<Integer> tree) {
	this.tree = tree;
	setTree();

	}

public void setTree() {
	this.setLayout(new BorderLayout());
	add(view, BorderLayout.CENTER);
	JPanel panel = new JPanel();
	panel.add(new JLabel("Enter a key:"));
	panel.add(textfield);
	panel.add(button1);
	panel.add(button2);
	add(panel, BorderLayout.SOUTH);

button1.addActionListener(new ActionListener() {

@Override
public void actionPerformed(ActionEvent e) {
	int number = Integer.parseInt(textfield.getText());
	if (tree.search(number)) {
	JOptionPane.showMessageDialog(null, "Number " + number + " is already in the tree.");
	} else {
	tree.insert(number);
	view.repaint();

	}
	}
	});

button2.addActionListener(new ActionListener() {

@Override
public void actionPerformed(ActionEvent e) {
	int number = Integer.parseInt(textfield.getText());
	if (!tree.search(number)) {
	JOptionPane.showMessageDialog(null, "Number " + number + " is not in the tree.");
	} else {
	tree.delete(number);
	view.repaint();

	}
	}
	});
	}

public class View extends JPanel {
	int radius = 25;
	int vGap = 100;

@Override
public void paintComponent(Graphics g) {
	super.paintComponent(g);
	if (tree.getRoot() != null) {
	displayTree(g, tree.getRoot(), getWidth() / 2, 30, getWidth() / 4);

	}
	}

public void displayTree(Graphics g, BST.TreeNode<Integer> root, int x, int y, int hGap) {
	g.drawOval(x - radius, y - radius, 2 * radius, 2 * radius);
	g.drawString(" " + root.element + " ", x - 6, y + 4);
	g.drawString(" " + tree.balanceFactor((AVLTree.AVLTreeNode<Integer>)(root)) + " ", x - 6, y + radius + 15);

	if (root.left != null) {
	connectCircles(g, x - hGap, y + vGap, x, y);
	displayTree(g, root.left, x - hGap, y + vGap, hGap / 2);

	}

	if (root.right != null) {
	connectCircles(g, x + hGap, y + vGap, x, y);
	displayTree(g, root.right, x + hGap, y + vGap, hGap / 2);

	}
	}

public void connectCircles(Graphics g, int x1, int y1, int x2, int y2) {
	int z = (int) (Math.sqrt(vGap * vGap + (x2 - x1) * (x2 - x1)));
	int x3 = (int) (x1 - radius * (x1 - x2) / z);
	int y3 = (int) (y1 - radius * (y1 - y2) / z);
	int x4 = (int) (x2 + radius * (x1 - x2) / z);
	int y4 = (int) (y2 + radius * (y1 - y2) / z);
	g.drawLine(x3, y3, x4, y4);

	}
	}

public static void main(String[] args) {
	JFrame frame = new JFrame();
	JApplet applet = new ViewTree();
	frame.add(applet);
	frame.setSize(400, 400);
	frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	frame.setLocationRelativeTo(null);
	frame.setVisible(true);

	}

}
